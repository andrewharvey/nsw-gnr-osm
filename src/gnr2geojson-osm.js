#!/usr/bin/env node

var fs = require('fs');
var csv = require('ya-csv');
var assert = require('assert');

var valueMap = {
    'REFERENCE': 'ref:nswgnb',
    'GAZETTE DATE': 'start_date:name',
    'PREVIOUS NAMES': 'old_name',
    'DESCRIPTION': 'description',
    'PLACENAME': 'name',
    'GEOGRAPHICAL NAME': 'alt_name',
    'ORIGIN': 'name:origin',
    'MEANING': 'name:meaning'
};

var schemaMap = {};
// http://www.gnb.nsw.gov.au/__data/assets/pdf_file/0011/59627/Glossary_of_Designation_Values.pdf
var schemaReader = csv.createCsvFileReader('src/gnrToOSMMapping.csv', {
    separator: '\t',
    columnsFromHeader: true
});
schemaReader.addListener('error', function(e) {
    console.error(e);
});
schemaReader.addListener('data', function (data) {
    schemaMap[data.designation] = data.tags ? data.tags.split(',') : null;
});

schemaReader.addListener('end', function (data) {
    var reader = csv.createCsvFileReader('dist/gnr.csv', {
        nestedQuotes: true,
        columnsFromHeader: true
    });
    reader.addListener('error', function(e) {
        console.error(e);
    });
    var features = [];
    reader.addListener('data', function (data) {
        var coordinates = [data['GDA2020 LONG'], data['GDA2020 LAT']];
        coordinates = coordinates.map((coordinate) => {
            return new Number(coordinate).toFixed(6);
        });

        var properties = {};

        Object.keys(valueMap).forEach(function(key,index) {
            var value = (data[key] || '').trim();
            var tagKey = valueMap[key];
            if (value) {
                if (tagKey == 'name') {
                    switch (data['STATUS'].toUpperCase()) {
                        case 'ABANDONED':
                            tagKey = 'abandoned:name';
                            break;
                        case 'ADVERTISED':
                            tagKey = 'planned:name';
                            break;
                        case 'ASSIGNED':
                        case 'ASSIGNED EDU':
                        case 'ASSIGNED NPWS':
                        case 'CONCUR':
                            break;
                        case 'DEFERRED':
                            tagKey = 'planned:name';
                            break;
                        case 'DISCONTINUED':
                            tagKey = 'historic:name';
                            break;
                        case 'DUAL PROPOSED':
                            tagKey = 'proposed:name';
                            break;
                        case 'DUAL - OFFICIAL ASSIGNED':
                           if (data['ABORIGINAL NAME'] === 'Yes') {
                             tagKey = 'name:aus';
                           }
                           break;
                        case 'DUAL ADVERT':
                            tagKey = 'planned:name';
                            break;
                        case 'PROPOSED':
                            tagKey = 'proposed:name';
                            break;
                        case 'RECORDED':
                            tagKey = 'name';
                            break;
                        case 'REJECTED':
                            tagKey = 'removed:name';
                            break;
                        case 'VARIANT':
                            tagKey = 'alt_name1';
                            break;
                        case 'WITHDRAWN':
                            tagKey = 'removed:name';
                            break;
                    }
                }
                properties[tagKey] = value;
            }
        });

        properties['nswgnb:status'] = data['STATUS'];

        var keys = schemaMap[data['DESIGNATION']];
        if (keys) {
            keys.forEach((key) => {
                var tag = key.split('=');
                assert(tag.length == 2, 'In gnrToOSMMapping.csv expected a key=value but that was not found.');
                properties[tag[0]] = tag[1];
            });
        }

        properties['nswgnb:designation'] = data['DESIGNATION'];

        if (data['DESIGNATION'] == 'TRIG. STATION') {
            var match = data['DESCRIPTION'].match('(TS[0-9]+)\s*\.?\s*$');
            if (match && match.length >= 2) {
                properties['ref'] = match[1];
            }
        }

        var feature = {
            type: 'Feature',
            properties: properties,
            geometry: (coordinates[0] === null || coordinates[1] === null) ? null : {
                type: 'Point',
                coordinates: coordinates
            }
        };
        features.push(feature);
    });

    reader.addListener('end', function (data) {
        var geojson = {
            type: 'FeatureCollection',
            features: features
        };

        fs.writeFileSync('dist/gnr.osm.geojson', JSON.stringify(geojson));
    });
});
