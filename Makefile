all: dist/gnr.osm

download_docs:
	wget http://www.gnb.nsw.gov.au/__data/assets/pdf_file/0011/59627/Glossary_of_Designation_Values.pdf
	wget http://www.gnb.nsw.gov.au/__data/assets/pdf_file/0003/59628/Glossary_of_Status_Values_GNB.pdf

dist/gnr.raw.csv:
	mkdir -p dist
	wget -O $@ https://dcok8xuap4.execute-api.ap-southeast-2.amazonaws.com/prod/public/placenames/geonames/download


dist/gnr.csv: dist/gnr.raw.csv
	cat $< | tail -n +5 > $@

dist/gnr.osm.geojson: dist/gnr.csv
	mkdir -p dist
	./src/gnr2geojson-osm.js

dist/gnr.osm: dist/gnr.osm.geojson
	mkdir -p dist
	./node_modules/.bin/geojson2osm $< > $@

clean:
	rm -f gnr.zip gnr.csv gnr.osm.geojson gnr.osm Glossary_of_Designation_Values_2017.pdf Glossary_of_Status_Values_GNB.pdf

previous_run.zip:
	wget -O $@ 'https://gitlab.com/api/v4/projects/5282366/jobs/artifacts/master/download?job=build'

prev: previous_run.zip
	unzip $< -d $@

diff: prev/dist/gnr.osm.geojson dist/gnr.osm.geojson
	./diff.js -o dist prev/dist/gnr.osm.geojson dist/gnr.osm.geojson

dist/new.xml: dist/new.geojson
	if test `ogrinfo -al -so $< | grep 'Feature Count' | cut -d' ' -f3` -gt 0 ; then ogr2ogr -f GeoRSS -sql 'SELECT name AS title, "start_date:name" || description AS description FROM new' -dialect sqlite  $@ $<; fi

dist/modified.xml: dist/modified.geojson
	if test `ogrinfo -al -so $< | grep 'Feature Count' | cut -d' ' -f3` -gt 0 ; then ogr2ogr -f GeoRSS -sql 'SELECT name AS title, "start_date:name" || description AS description FROM modified' -dialect sqlite  $@ $<; fi

dist/deleted.xml: dist/deleted.geojson
	if test `ogrinfo -al -so $< | grep 'Feature Count' | cut -d' ' -f3` -gt 0 ; then ogr2ogr -f GeoRSS -sql 'SELECT name AS title, "start_date:name" || description AS description FROM deleted' -dialect sqlite  $@ $<; fi
