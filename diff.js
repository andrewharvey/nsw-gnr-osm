#!/usr/bin/env node

const argv = require('minimist')(process.argv.slice(2))
const deepEqual = require('deep-equal')
const fs = require('fs')
const path = require('path')
const featureCollection = require('@turf/helpers').featureCollection

const a = JSON.parse(fs.readFileSync(argv._[0]))
const b = JSON.parse(fs.readFileSync(argv._[1]))

const id = 'ref:nswgnb'

const ao = a.features.reduce((obj, item) => {
    obj[item.properties[id]] = item
    return obj
}, {})

const newFeatures = []
const modifiedFeatures = []
const deletedFeatures = []

b.features.map((featureB) => {
    if (featureB.properties[id] in ao) {
        // found in a, see if properties match
        if (deepEqual(ao[featureB.properties[id]], featureB)) {
            // equal, no change
        } else {
            modifiedFeatures.push(featureB)
        }
    } else {
        // not found in a, must be new
        newFeatures.push(featureB)
    }
})

const bo = b.features.reduce((obj, item) => {
    obj[item.properties[id]] = item
    return obj
}, {})

a.features.map((featureA) => {
    if (featureA.properties[id] in bo) {
        // found
    } else {
        // not found in b, must be deleted
        deletedFeatures.push(featureA)
    }
})

fs.writeFileSync(path.join(argv.o, 'new.geojson'), JSON.stringify(featureCollection(newFeatures)))
fs.writeFileSync(path.join(argv.o, 'modified.geojson'), JSON.stringify(featureCollection(modifiedFeatures)))
fs.writeFileSync(path.join(argv.o, 'deleted.geojson'), JSON.stringify(featureCollection(deletedFeatures)))
